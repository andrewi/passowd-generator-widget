<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Password Generator</title>
	<link type="text/css" rel="stylesheet" href="/Widgets/PasswordGenerator/Resources/Style/password-generator-widget.css">
</head>
<body>

	<?php
		require_once 'Widgets/PasswordGenerator/password-generator.php';
		$passwordGenerator = new \Widgets\PasswordGenerator;

		$widget = $passwordGenerator
			->create('pwd_gen_id')
			->addResponseListener()
			->display();
	?>

</body>
</html>

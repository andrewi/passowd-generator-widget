<div class="generator-widget">

	<div class="generator-widget-title">
		<h3>Password Generator Widget</h3>
	</div>

	<?php
		$errors = $this->generator->getErrors();
		if ( ! empty( $errors ) ) {
	?>
		<div class="errors error-list">
			<?php foreach ( $errors as $error ) { ?>
				<div class="error">
					<i><?php echo $error; ?></i>
				</div>
			<?php } ?>
		</div>
	<?php } ?>

	<form class="password-generator-form" action="/" method="get">
		<label>
			<span>Password length</span>
			<input type="text" name="<?php echo $this->generator->getId(); ?>[length]" value="<?php echo $this->generator->getPasswordLength(); ?>">
		</label>
		<label>
			<input type="checkbox" name="<?php echo $this->generator->getId(); ?>[useUniqChars]" value="1" <?php echo ( $this->generator->getUseUniqChars() ? 'checked="checked"' : '' ); ?> >
			<span>Use unique characters</span>
		</label><br>

		<?php foreach ( $this->generator->getAvailableCharTypes() as $charType ) { ?>
			<label>
				<input type="checkbox" name="<?php echo $this->generator->getId(); ?>[<?php echo $charType ?>]" value="1" <?php echo ( in_array( $charType, $this->generator->getCharTypes() ) ? 'checked="checked"' : '' ); ?> >
				<span>Use <?php echo $charType ?> characters</span>
			</label>
		<?php } ?>
		<input type="submit" value="Generate">
	</form>

	<?php
		$password = $this->generator->getPassword();
		if ( ! empty( $password ) ) {
	?>
		<div class="generatedPassword">
			<strong class="password"><?php echo $password ?></strong>
			<span class="small">Password generated</span>
		</div>
	<?php } ?>

</div>

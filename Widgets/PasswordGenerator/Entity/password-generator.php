<?php
/**
 * Author: Andrew Ivanov
 * Author e-mail: andrewi.wd@gmail.com
 * License: GPL
 */

namespace Widgets\PasswordGenerator\Entity;

class PasswordGeneratorEntity
{

	const MIN_PASSWORD_LENGTH = 4;
	const MAX_PASSWORD_LENGTH = 100;

	private $id;

	private $availableCharsTypes = array( 'lowercase', 'uppercase', 'digit' );
	private $usedCharTypes = array( 'lowercase', 'uppercase', 'digit' );
	private $useUniqChars = true;

	private $lowercaseChars;
	private $uppercaseChars;
	private $digitChars;

	private $minCharCount;
	private $maxCharCount;

	private $password;
	private $passwordChars = array();
	private $passwordLength;

	private $errors = array();

	public function setId( $id ) {
		$id = preg_replace( '@[^a-z0-9_]+@', '_', $id );
		$this->id = $id;
		return $this;
	}

	public function getId() {
		return $this->id;
	}

	public function addCharType( $charType ) {
		if ( ! in_array( $charType, $this->availableCharsTypes ) ) {
			$this->addError('Undefined char type');
		}
		if ( ! in_array( $charType, $this->usedCharTypes ) ) {
			$this->usedCharTypes[] = $charType;
		}
		return $this;
	}

	public function removeCharType( $charType ) {
		if ( ( $key = array_search( $charType, $this->usedCharTypes ) ) !== false ) {
			unset( $this->usedCharTypes[ $key ] );
		}
		return $this;
	}

	public function getCharTypes() {
		return $this->usedCharTypes;
	}

	public function getUsedCharTypes() {
		return $this->getCharTypes();
	}

	public function getAvailableCharTypes() {
		return $this->availableCharsTypes;
	}

	public function setLowercaseChars( $chars = array() ) {
		$this->lowercaseChars = $chars;
		return $this;
	}

	public function getLowercaseChars() {
		return $this->lowercaseChars;
	}

	public function removeLowercaseChar( $char ) {
		if ( ( $key = array_search( $char, $this->lowercaseChars ) ) !== false ) {
			unset( $this->lowercaseChars[ $key ] );
		}
		return $this;
	}

	public function setUppercaseChars( $chars = array() ) {
		$this->uppercaseChars = $chars;
		return $this;
	}

	public function getUppercaseChars() {
		return $this->uppercaseChars;
	}

	public function removeUppercaseChar( $char ) {
		if ( ( $key = array_search( $char, $this->uppercaseChars ) ) !== false ) {
			unset( $this->uppercaseChars[ $key ] );
		}
		return $this;
	}

	public function setDigitChars( $chars = array() ) {
		$this->digitChars = $chars;
		return $this;
	}

	public function getDigitChars() {
		return $this->digitChars;
	}

	public function removeDigitChar( $char ) {
		if ( ( $key = array_search( $char, $this->digitChars ) ) !== false ) {
			unset( $this->digitChars[ $key ] );
		}
		return $this;
	}

	public function getChars( $type ) {
		switch ( $type ) {
			case 'lowercase' :
				return $this->getLowercaseChars();
				break;
			case 'uppercase' :
				return $this->getUppercaseChars();
				break;
			case 'digit' :
				return $this->getDigitChars();
				break;
		}
		return array();
	}

	public function getChar( $type ) {
		switch ( $type ) {
			case 'lowercase' :
				$charsArray = $this->getLowercaseChars();
				$char = $charsArray[ array_rand( $charsArray, 1 ) ];
				return $char;
				break;
			case 'uppercase' :
				$charsArray = $this->getUppercaseChars();
				$char = $charsArray[ array_rand( $charsArray, 1 ) ];
				return $char;
				break;
			case 'digit' :
				$charsArray = $this->getDigitChars();
				$char = $charsArray[ array_rand( $charsArray, 1 ) ];
				return $char;
				break;
		}
		return array();
	}

	public function removeChar( $type, $char ) {
		switch ( $type ) {
			case 'lowercase' :
				$this->removeLowercaseChar( $char );
				break;
			case 'uppercase' :
				$this->removeUppercaseChar( $char );
				break;
			case 'digit' :
				$this->removeDigitChar( $char );
				break;
		}
		return $this;
	}

	public function setMinCharCount( $limit = self::MIN_PASSWORD_LENGTH ) {
		$minLimit = count( $this->getCharTypes() );
		$this->minCharCount = ( (int) $limit >= $minLimit ? (int) $limit : (int) $minLimit );
		return $this;
	}

	public function getMinCharCount() {
		return $this->minCharCount;
	}

	public function setMaxCharCount( $limit = self::MAX_PASSWORD_LENGTH ) {
		$maxLimit = 0;
		foreach ( $this->getCharTypes() as $type ) {
			$maxLimit += count( $this->getChars( $type ) );
		}
		$this->maxCharCount = ( (int) $limit <= $maxLimit ? (int) $limit : (int) $maxLimit );
		if ( false === $this->useUniqChars ) {
			$this->maxCharCount = ( (int) $limit <= self::MAX_PASSWORD_LENGTH ? (int) $limit : (int) self::MAX_PASSWORD_LENGTH );
		}
		return $this;
	}

	public function getMaxCharCount() {
		return $this->maxCharCount;
	}

	public function setPasswordLength( $length ) {
		if ( (int) $length > $this->maxCharCount || (int) $length < $this->minCharCount ) {
			$this->addError('Password length must be between ' . $this->minCharCount . ' and ' . $this->maxCharCount . ' characters');
		}
		$this->passwordLength = (int) $length;
		return $this;
	}

	public function getPasswordLength() {
		return $this->passwordLength;
	}

	public function addPasswordChar( $char ) {
		$this->passwordChars[] = $char;
		return $this;
	}

	public function getPasswordChars() {
		return $this->passwordChars;
	}

	public function setPassword( $password ) {
		$this->password = $password;
		return $this;
	}

	public function getPassword() {
		return $this->password;
	}

	public function setUseUniqChars( $useUniqChars = true ) {
		$this->useUniqChars = (bool) $useUniqChars;
		return $this;
	}

	public function getUseUniqChars() {
		return $this->useUniqChars;
	}

	public function addError( $message ) {
		$this->errors[] = $message;
		return $this;
	}

	public function getErrors() {
		return $this->errors;
	}

}

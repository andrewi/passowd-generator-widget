<?php
/**
 * Author: Andrew Ivanov
 * Author e-mail: andrewi.wd@gmail.com
 * License: GPL
 */

namespace Widgets;

require 'Entity/password-generator.php';
use Widgets\PasswordGenerator\Entity\PasswordGeneratorEntity as GeneratorEntity;

class PasswordGenerator
{

	/**
	 * @var Widgets\PasswordGenerator\Entity\PasswordGeneratorEntity
	 */
	public $generator;

	/**
	 * Creates Password generators Object with default configuration
	 * @param  string $id Human readable widgets id
	 */
	public function create( $id = null ) {
		if ( empty( $id ) ) {
			throw new \Exception( 'Password generators widget must have an uniq ID', 1 );
		}
		$this->generator = new GeneratorEntity;
		$this->generator
			->setId( $id )
			->setLowercaseChars( range( 'a', 'z' ) )
			->setUppercaseChars( range( 'A', 'Z') )
			->setDigitChars( range( 0, 9 ) )
			->setUseUniqChars( true )
			->setMinCharCount( 3 )
			->setMaxCharCount( 62 )
			->setPasswordLength( 8 );
		return $this;
	}

	/**
	 * Listen for servers response to set up PasswordGenerator human preferences.
	 * If prefenreces are setted up it generates new password.
	 * Currently using only global $_GET
	 * @todo Add Form entity with acceptable methods, routes, encoding etc. Modify view file.
	 */
	public function addResponseListener() {
		$id = $this->generator->getId();
		if ( ! empty( $_GET[ $id ] ) ) {
			$widgetData = $_GET[ $id ];
			foreach ( $this->generator->getAvailableCharTypes() as $charType ) {
				if ( empty( $widgetData[ $charType ] ) || $widgetData[ $charType ] != 1 ) {
					$this->generator->removeCharType( $charType );
				}
			}
			if ( empty( $widgetData[ 'useUniqChars' ] ) || $widgetData[ 'useUniqChars' ] != 1 ) {
				$this->generator->setUseUniqChars( false );
			}
			if ( ! empty( $widgetData['length'] ) ) {
				if ( 0 === count( $this->generator->getCharTypes() ) ) {
					$this->generator->addError('No character types selected');
					return $this;
				}
				$this->generator->setMinCharCount();
				$this->generator->setMaxCharCount();
				$this->generator->setPasswordLength( $widgetData['length'] );
			}
			$this->generatePassword();
		}
		return $this;
	}

	/**
	 * Collect and render generated HTML code from the View file.
	 */
	public function display() {
		ob_start();
		include 'Resources/View/password-generator.html.php';
		$template = ob_get_contents();
		ob_end_clean();
		echo $template;
	}

	/**
	 * Generates password according to current Generator object.
	 * Do not generates password if errors are found.
	 */
	private function generatePassword() {
		if ( count( $this->generator->getErrors() ) > 0 ) {
			return $this;
		}
		$this->addPasswordChars( $this->generator );
		$this->generator->setPassword( implode( '', $this->shuffle( $this->generator->getPasswordChars() ) ) );
		return $this;
	}

	/**
	 * Adds uniq characters from selected Character arrays to GeneratorEntity password array.
	 * At first, adds from each selected Character arrays one char. In case password to contain at least
	 * 1 character from each Character arrays.
	 * For rest chars getting random chars from random selected Character arrays to fully fill password length.
	 * @param object $generator GeneratorEntity
	 */
	private function addPasswordChars( &$generator ) {
		$length = $generator->getPasswordLength();
		$usedTypes = array_values( $generator->getCharTypes() );
		$usedTypesCount = count( $usedTypes );

		foreach ( $usedTypes as $usedType ) {
			$char = $generator->getChar( $usedType );
			$generator->addPasswordChar( $char );
			if ( $generator->getUseUniqChars() ) {
				$generator->removeChar( $usedType, $char );
			}
			$length--;
		}

		for ( $i = 0; $i < $length; $i++ ) {
			$randomTypeKey = rand( 0, $usedTypesCount - 1 );
			$chars = $generator->getChars( $usedTypes[ $randomTypeKey ] );
			while ( empty( $chars ) ) {
				unset( $usedTypes[ $randomTypeKey ] );
				$usedTypes = array_values( $usedTypes );
				$usedTypesCount = count( $usedTypes );
				$randomTypeKey = rand( 0, $usedTypesCount - 1 );
				$chars = $generator->getChars( $usedTypes[ $randomTypeKey ] );
			}
			$char = $generator->getChar( $usedTypes[ $randomTypeKey ] );
			$generator->addPasswordChar( $char );
			if ( $generator->getUseUniqChars() ) {
				$generator->removeChar( $usedType, $char );
			}
		}
	}

	/**
	 * Implements basic PHP shuffle function, to return an array not boolean as origin function do
	 * @param  array $array
	 * @return array
	 */
	private function shuffle( $array ) {
		shuffle( $array );
		return $array;
	}

}
